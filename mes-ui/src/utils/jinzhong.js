// 回显数据
export function selectDictLabel(datas, value) {
  if (value === undefined) {
    return "";
  }
  var actions = [];

  Object.keys(datas).some((key) => {

    if (datas[key].id == ('' + value)) {
      actions.push(datas[key].name);
      return true;
    }
  })
  if (actions.length === 0) {
    actions.push(value);
  }
  return actions.join('');
}

