package cn.sourceplan.production.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.sourceplan.production.domain.WorkOrderEntry;
import org.springframework.stereotype.Repository;

/**
 * 工单Mapper接口
 *
 * @author jinzhong
 * @date 2022-12-12
 */
@Repository
public interface WorkOrderEntryMapper extends BaseMapper<WorkOrderEntry>
{

}
