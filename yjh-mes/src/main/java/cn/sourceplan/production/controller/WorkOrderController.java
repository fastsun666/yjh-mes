package cn.sourceplan.production.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.sourceplan.common.annotation.Log;
import cn.sourceplan.common.core.controller.BaseController;
import cn.sourceplan.common.core.domain.AjaxResult;
import cn.sourceplan.common.enums.BusinessType;
import cn.sourceplan.production.domain.WorkOrder;
import cn.sourceplan.production.service.IWorkOrderService;
import cn.sourceplan.common.utils.poi.ExcelUtil;
import cn.sourceplan.common.core.page.TableDataInfo;

/**
 * 工单Controller
 *
 * @author jinzhong
 * @date 2022-12-12
 */
@RestController
@RequestMapping("/production/workOrder")
public class WorkOrderController extends BaseController
{
    @Autowired
    private IWorkOrderService workOrderService;

    /**
     * 查询工单列表
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(WorkOrder workOrder)
    {
        String proStatus = "";
        if(org.apache.commons.lang3.StringUtils.isNotBlank(workOrder.getProStatus())){
            String[] split = workOrder.getProStatus().split(",");
            for (int i = 0; i < split.length; i++) {
                proStatus+="'"+split[i]+"'";
                if(i!=split.length-1){
                    proStatus+=",";
                }
            }
        }
        workOrder.setProStatus(proStatus);
        TableDataInfo tableDataInfo = workOrderService.selectWorkOrderList(workOrder);
        return tableDataInfo;
    }

    /**
     * 移动端查询工单列表
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:list')")
    @GetMapping("/listForMobile")
    public TableDataInfo listForMobile(WorkOrder workOrder)
    {
        return  workOrderService.listForMobile(workOrder);
    }

    /**
     * 导出工单列表
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:export')")
    @Log(title = "工单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WorkOrder workOrder)
    {
        TableDataInfo tableDataInfo = workOrderService.selectWorkOrderList(workOrder);
        List<WorkOrder> rows = (List<WorkOrder>)tableDataInfo.getRows();
        ExcelUtil<WorkOrder> util = new ExcelUtil<WorkOrder>(WorkOrder.class);
        util.exportExcel(response, rows, "工单数据");
    }

    /**
     * 获取工单详细信息
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(workOrderService.selectWorkOrderById(id));
    }

    /**
     * 根据工单子表ID获取工单详细信息
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:query')")
    @GetMapping(value = "/getInfoByEntryId/{id}")
    public AjaxResult getInfoByEntryId(@PathVariable("id") Long id)
    {
        return success(workOrderService.selectWorkOrderByEntryId(id));
    }


    /**
     * 新增工单
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:add')")
    @Log(title = "工单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WorkOrder workOrder)
    {
        return toAjax(workOrderService.insertWorkOrder(workOrder));
    }


    /**
     * 批量新增工单
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:add')")
    @Log(title = "工单", businessType = BusinessType.INSERT)
    @PostMapping("/batchAdd")
    public AjaxResult batchAdd(@RequestBody List<WorkOrder> workOrderList)
    {
        return toAjax(workOrderService.batchInsertWorkOrder(workOrderList));
    }

    /**
     * 修改工单
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:edit')")
    @Log(title = "工单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WorkOrder workOrder)
    {
        return workOrderService.updateWorkOrder(workOrder);
    }

    /**
     * 删除工单
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:remove')")
    @Log(title = "工单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return workOrderService.deleteWorkOrderByIds(ids);
    }

    /**
     * 批量修改生产状态
     */
    @PutMapping("/editProStatus")
    public AjaxResult editProStatus(Long[] ids,String proStatus)
    {
        return workOrderService.updateProStatus(ids,proStatus);
    }

    /**
     * 工单排产计划预览
     */
    @PreAuthorize("@ss.hasPermi('production:workOrder:list')")
    @GetMapping("/preview")
    public AjaxResult preview(String ids,String billType)
    {
        if(ids.split(",").length<1){
            return AjaxResult.error("所选单据ID有异常，请联系管理员处理");
        }
        return workOrderService.preview(ids,billType);
    }

}
