package cn.sourceplan.production.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.sourceplan.common.annotation.Log;
import cn.sourceplan.common.core.controller.BaseController;
import cn.sourceplan.common.core.domain.AjaxResult;
import cn.sourceplan.common.enums.BusinessType;
import cn.sourceplan.production.domain.Report;
import cn.sourceplan.production.service.IReportService;
import cn.sourceplan.common.utils.poi.ExcelUtil;
import cn.sourceplan.common.core.page.TableDataInfo;

/**
 * 报工单Controller
 *
 * @author jinzhong
 * @date 2023-02-15
 */
@RestController
@RequestMapping("/production/report")
public class ReportController extends BaseController
{
    @Autowired
    private IReportService reportService;

    /**
     * 查询报工单列表
     */
    @PreAuthorize("@ss.hasPermi('production:report:list')")
    @GetMapping("/list")
    public TableDataInfo list(Report report)
    {
        startPage();
        List<Report> list = reportService.selectReportList(report);
        return getDataTable(list);
    }

    /**
     * 导出报工单列表
     */
    @PreAuthorize("@ss.hasPermi('production:report:export')")
    @Log(title = "报工单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Report report)
    {
        List<Report> list = reportService.selectReportList(report);
        ExcelUtil<Report> util = new ExcelUtil<Report>(Report.class);
        util.exportExcel(response, list, "报工单数据");
    }

    /**
     * 获取报工单详细信息
     */
    @PreAuthorize("@ss.hasPermi('production:report:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(reportService.selectReportById(id));
    }

    /**
     * 批量新增/保存报工单
     */
    @PreAuthorize("@ss.hasPermi('production:report:add')")
    @Log(title = "报工单", businessType = BusinessType.INSERT)
    @PostMapping("/batchSave")
    public AjaxResult batchSave(@RequestBody List<Report> reportList)
    {
        int count =0;
        for (int i = 0; i < reportList.size(); i++) {
            count+=reportService.insertReport(reportList.get(i));
        }

        return toAjax(count);
    }

    /**
     * 新增报工单
     */
    @PreAuthorize("@ss.hasPermi('production:report:add')")
    @Log(title = "报工单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody List<Report> reportList)
    {
        Report report = reportList.get(0);
        int i = reportService.insertReport(report);
        return toAjax(i);
    }


    /**
     * 修改报工单
     */
    @PreAuthorize("@ss.hasPermi('production:report:edit')")
    @Log(title = "报工单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Report report)
    {

        return reportService.updateReport(report);
    }

    /**
     * 删除报工单
     */
    @PreAuthorize("@ss.hasPermi('production:report:remove')")
    @Log(title = "报工单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return reportService.deleteReportByIds(ids);
    }
}
