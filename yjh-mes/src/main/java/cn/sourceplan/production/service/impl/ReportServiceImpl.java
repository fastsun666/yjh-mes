package cn.sourceplan.production.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.sourceplan.barcode.mapper.MaterialSnMapper;
import cn.sourceplan.common.core.domain.AjaxResult;
import cn.sourceplan.common.domain.SysCodeRule;
import cn.sourceplan.common.service.ISysCodeRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.sourceplan.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import cn.sourceplan.production.mapper.ReportMapper;
import cn.sourceplan.production.domain.Report;
import cn.sourceplan.production.service.IReportService;

/**
 * 报工单Service业务层处理
 *
 * @author jinzhong
 * @date 2023-02-15
 */
@Service
public class ReportServiceImpl implements IReportService
{
    @Autowired
    private ReportMapper reportMapper;
    @Autowired
    private ISysCodeRuleService sysCodeRuleService;

    @Autowired
    private MaterialSnMapper materialSnMapper;

    /**
     * 查询报工单
     *
     * @param id 报工单主键
     * @return 报工单
     */
    @Override
    public Report selectReportById(Long id)
    {
        return reportMapper.selectById(id);
    }

    /**
     * 查询报工单列表
     *
     * @param report 报工单
     * @return 报工单
     */
    @Override
    public List<Report> selectReportList(Report report)
    {
        Map<String, Object> params = report.getParams();
        QueryWrapper<Report> qw =new QueryWrapper<>();
        //封装过滤条件
        if(StringUtils.isNotBlank(report.getNumber() )){
            qw.like("number",report.getNumber());
        }
        if(StringUtils.isNotBlank(report.getProcessName() )){
            qw.like("process_name",report.getProcessName());
        }
        if(StringUtils.isNotBlank(report.getWorkOrderNumber() )){
            qw.like("workOrderNumber",report.getWorkOrderNumber());
        }
        if(StringUtils.isNotBlank( report.getWorkOrderName())){
            qw.like("workOrderName",report.getWorkOrderName());
        }
        if(StringUtils.isNotBlank(report.getMaterialName() )){
            qw.like("materialName",report.getMaterialName());
        }
        if(StringUtils.isNotBlank(report.getReportUserName() )){
            qw.like("report_user_name",report.getReportUserName());
        }
        if(StringUtils.isNotBlank(report.getReportChannel() )){
            qw.eq("report_channel",report.getReportChannel());
        }
        if(report.getWorkOrderEntryId()!=null){
            qw.eq("work_order_entry_id",report.getWorkOrderEntryId());
        }
        if(params!=null&&params.get("workOrderEntryIds")!=null&&StringUtils.isNotBlank (String.valueOf(params.get("workOrderEntryIds")))  ){
            qw.inSql("work_order_entry_id",String.valueOf(params.get("workOrderEntryIds")));
        }
        if(StringUtils.isNotBlank(report.getQualityStatus() )){
            qw.eq("quality_status",report.getQualityStatus());
        }
        if(StringUtils.isNotBlank(report.getStatus() )){
            qw.eq("status",report.getStatus());
        }
        qw.orderByDesc("report_time");
        return reportMapper.selectReportList(qw);
    }

    /**
     * 新增报工单
     *
     * @param report 报工单
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertReport(Report report)
    {
        //如果编号为null,则自动为其生成编号
        if ( StringUtils.isBlank(report.getNumber()) ){
            SysCodeRule queryDomain  = new SysCodeRule();
            queryDomain.setBasicDomain("Report");
            List<SysCodeRule> sysCodeRules = sysCodeRuleService.selectSysCodeRuleList(queryDomain);
            String number = sysCodeRuleService.queryNewCodeById(sysCodeRules.get(0).getId(),true);
            report.setNumber(number);
        }
        int rows = reportMapper.insert(report);
        //更新SN码追溯表中的内容
        String snArray = report.getSnArray();
        if(StringUtils.isNotBlank(snArray)){
            int i = snArray.lastIndexOf(",");
            snArray = snArray.substring(0,i);
            String SnContent = DateUtil.now()+"： "+report.getReportUserName()+"-进行报工操作("+report.getNumber()+")<br/> ";
            Map<String,Object> map = new HashMap<>();
            map.put("content",SnContent);
            map.put("whereSql", " and sn_code in(" +snArray+")");
            materialSnMapper.updateBatchSomeColumn(map);
        }
        return rows;
    }

    /**
     * 修改报工单
     *
     * @param report 报工单
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult updateReport(Report report)
    {
        int i = reportMapper.updateById(report);
        return i > 0 ? AjaxResult.success() : AjaxResult.error();
    }

    /**
     * 批量删除报工单
     *
     * @param ids 需要删除的报工单主键
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public AjaxResult deleteReportByIds(Long[] ids)
    {
        int i = reportMapper.deleteReportByIds(ids);
        return i > 0 ? AjaxResult.success(i) : AjaxResult.error();
    }


}
