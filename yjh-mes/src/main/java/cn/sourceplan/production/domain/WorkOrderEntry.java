package cn.sourceplan.production.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.sourceplan.common.annotation.Excel;
import cn.sourceplan.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 工单分录对象 pro_workorder_entry
 *
 * @author jinzhong
 * @date 2022-12-12
 */
@Data
@TableName("pro_workorder_entry")
public class WorkOrderEntry
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type= IdType.AUTO)
    private Long id;

    /** 车间ID */
    @Excel(name = "车间ID")
    private Long workshopId;

    /** 车间名称 */
    @Excel(name = "车间名称")
    private String workshopName;

    /** 工位ID */
    @Excel(name = "工位ID")
    private Long stationId;

    /** 工位名称 */
    @Excel(name = "工位名称")
    private String stationName;

    /** 工单ID */
    @Excel(name = "工单ID")
    private Long workorderId;

    /** 工序ID */
    @Excel(name = "工序ID")
    private Long processId;

    /** 工序名称 */
    @Excel(name = "工序名称")
    private String processName;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 质检类型 */
    @Excel(name = "质检类型")
    private String qcType;

    /** 首件数量 */
    @Excel(name = "首检数量")
    private BigDecimal firstQcQuantity;

}
