package cn.sourceplan.sale.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.sourceplan.sale.domain.SalOrderEntry;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author jinzhong
 */
@Repository
public interface SalOrderEntryMapper extends BaseMapper<SalOrderEntry> {


    /**
     * 批量插入 仅适用于mysql
     * @param entityList 实体列表
     * @return 影响行数
     */
    Integer insertBatchSomeColumn(Collection<SalOrderEntry> entityList);
}
