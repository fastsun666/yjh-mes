package cn.sourceplan.sale.service;

import java.util.List;
import cn.sourceplan.sale.domain.SalOrder;

/**
 * 销售订单Service接口
 *
 * @author jinzhong
 * @date 2022-10-08
 */
public interface ISalOrderService
{
    /**
     * 查询销售订单
     *
     * @param id 销售订单主键
     * @return 销售订单
     */
    SalOrder selectSalOrderById(Long id);

    /**
     * 查询销售订单列表
     *
     * @param salOrder 销售订单
     * @return 销售订单集合
     */
    List<SalOrder> selectSalOrderList(SalOrder salOrder);

    /**
     * 新增销售订单
     *
     * @param salOrder 销售订单
     * @return 结果
     */
    int insertSalOrder(SalOrder salOrder);

    /**
     * 修改销售订单
     *
     * @param salOrder 销售订单
     * @return 结果
     */
    int updateSalOrder(SalOrder salOrder);

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的销售订单主键集合
     * @return 结果
     */
    int deleteSalOrderByIds(Long[] ids);


}
