package cn.sourceplan.sale.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import cn.sourceplan.sale.mapper.SalOrderEntryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sourceplan.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import cn.sourceplan.sale.domain.SalOrderEntry;
import cn.sourceplan.sale.mapper.SalOrderMapper;
import cn.sourceplan.sale.domain.SalOrder;
import cn.sourceplan.sale.service.ISalOrderService;

/**
 * 销售订单Service业务层处理
 *
 * @author jinzhong
 * @date 2022-10-08
 */
@Service
public class SalOrderServiceImpl implements ISalOrderService
{
    @Autowired
    private SalOrderMapper salOrderMapper;

    @Autowired
    private SalOrderEntryMapper salOrderEntryMapper;

    /**
     * 查询销售订单
     *
     * @param id 销售订单主键
     * @return 销售订单
     */
    @Override
    public SalOrder selectSalOrderById(Long id)
    {
        SalOrder salOrder = salOrderMapper.selectSalOrderById(id);
        return salOrder;
    }

    /**
     * 查询销售订单列表
     *
     * @param salOrder 销售订单
     * @return 销售订单
     */
    @Override
    public List<SalOrder> selectSalOrderList(SalOrder salOrder)
    {
        String[] saleDateQuery = salOrder.getSaleDateQuery();
        if (saleDateQuery!=null&&saleDateQuery.length>0){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Map<String,Object> map = salOrder.getParams();
            map.put("saleDateStart",saleDateQuery[0]);
            if(saleDateQuery.length>1){
                map.put("saleDateEnd",saleDateQuery[1]);
            }else{
                String format = sdf.format(new Date());
                map.put("saleDateEnd",format);
            }
        }


        return salOrderMapper.selectSalOrderList(salOrder);
    }

    /**
     * 新增销售订单
     *
     * @param salOrder 销售订单
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertSalOrder(SalOrder salOrder)
    {
        int rows = salOrderMapper.insert(salOrder);
        insertSalOrderEntry(salOrder);
        return rows;
    }

    /**
     * 修改销售订单
     *
     * @param salOrder 销售订单
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateSalOrder(SalOrder salOrder)
    {
        salOrderMapper.deleteSalOrderEntryByMainId(salOrder.getId());
        insertSalOrderEntry(salOrder);
        return salOrderMapper.updateById(salOrder);
    }

    /**
     * 批量删除销售订单
     *
     * @param ids 需要删除的销售订单主键
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteSalOrderByIds(Long[] ids)
    {
        salOrderMapper.deleteSalOrderEntryByMainIds(ids);
        return salOrderMapper.deleteSalOrderByIds(ids);
    }


    /**
     * 新增${subTable.functionName}信息
     *
     * @param salOrder 销售订单对象
     */
    public void insertSalOrderEntry(SalOrder salOrder)
    {
        List<SalOrderEntry> salOrderEntryList = salOrder.getSalOrderEntryList();
        Long id = salOrder.getId();
        if (StringUtils.isNotNull(salOrderEntryList))
        {
            List<SalOrderEntry> list = new ArrayList<SalOrderEntry>();
            for (SalOrderEntry salOrderEntry : salOrderEntryList)
            {
                salOrderEntry.setMainId(id);
                list.add(salOrderEntry);
            }
            if (list.size() > 0)
            {
                salOrderEntryMapper.insertBatchSomeColumn(list);
            }
        }
    }
}
