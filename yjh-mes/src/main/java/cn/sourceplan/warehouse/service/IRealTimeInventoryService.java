package cn.sourceplan.warehouse.service;

import java.math.BigDecimal;
import java.util.List;
import cn.sourceplan.warehouse.domain.RealTimeInventory;

/**
 * 即时库存Service接口
 *
 * @author jinzhong
 * @date 2022-11-29
 */
public interface IRealTimeInventoryService
{
    /**
     * 查询即时库存
     *
     * @param aa 即时库存主键
     * @return 即时库存
     */
    public RealTimeInventory selectRealTimeInventoryByAa(BigDecimal aa);

    /**
     * 查询即时库存列表
     *
     * @param realTimeInventory 即时库存
     * @return 即时库存集合
     */
    public List<RealTimeInventory> selectRealTimeInventoryList(RealTimeInventory realTimeInventory);


}
