package cn.sourceplan.warehouse.service.impl;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.sourceplan.warehouse.mapper.RealTimeInventoryMapper;
import cn.sourceplan.warehouse.domain.RealTimeInventory;
import cn.sourceplan.warehouse.service.IRealTimeInventoryService;

/**
 * 即时库存Service业务层处理
 *
 * @author jinzhong
 * @date 2022-11-29
 */
@Service
public class RealTimeInventoryServiceImpl implements IRealTimeInventoryService
{
    @Autowired
    private RealTimeInventoryMapper realTimeInventoryMapper;

    /**
     * 查询即时库存
     *
     * @param aa 即时库存主键
     * @return 即时库存
     */
    @Override
    public RealTimeInventory selectRealTimeInventoryByAa(BigDecimal aa)
    {
        return realTimeInventoryMapper.selectRealTimeInventoryByAa(aa);
    }

    /**
     * 查询即时库存列表
     *
     * @param realTimeInventory 即时库存
     * @return 即时库存
     */
    @Override
    public List<RealTimeInventory> selectRealTimeInventoryList(RealTimeInventory realTimeInventory)
    {
        return realTimeInventoryMapper.selectRealTimeInventoryList(realTimeInventory);
    }


}
