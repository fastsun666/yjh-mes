package cn.sourceplan.warehouse.mapper;

import java.math.BigDecimal;
import java.util.List;
import cn.sourceplan.warehouse.domain.RealTimeInventory;
import org.springframework.stereotype.Repository;

/**
 * 即时库存Mapper接口
 *
 * @author jinzhong
 * @date 2022-11-29
 */
@Repository
public interface RealTimeInventoryMapper
{
    /**
     * 查询即时库存
     *
     * @param aa 即时库存主键
     * @return 即时库存
     */
    RealTimeInventory selectRealTimeInventoryByAa(BigDecimal aa);

    /**
     * 查询即时库存列表
     *
     * @param realTimeInventory 即时库存
     * @return 即时库存集合
     */
    List<RealTimeInventory> selectRealTimeInventoryList(RealTimeInventory realTimeInventory);



}
