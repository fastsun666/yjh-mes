package cn.sourceplan.warehouse.controller;

import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.sourceplan.common.annotation.Log;
import cn.sourceplan.common.core.controller.BaseController;
import cn.sourceplan.common.core.domain.AjaxResult;
import cn.sourceplan.common.enums.BusinessType;
import cn.sourceplan.warehouse.domain.RealTimeInventory;
import cn.sourceplan.warehouse.service.IRealTimeInventoryService;
import cn.sourceplan.common.utils.poi.ExcelUtil;
import cn.sourceplan.common.core.page.TableDataInfo;

/**
 * 即时库存Controller
 *
 * @author jinzhong
 * @date 2022-11-29
 */
@RestController
@RequestMapping("/warehouse/inventory")
public class RealTimeInventoryController extends BaseController
{
    @Autowired
    private IRealTimeInventoryService realTimeInventoryService;

    /**
     * 查询即时库存列表
     */
    @PreAuthorize("@ss.hasPermi('warehouse:inventory:list')")
    @GetMapping("/list")
    public TableDataInfo list(RealTimeInventory realTimeInventory)
    {
        startPage();
        List<RealTimeInventory> list = realTimeInventoryService.selectRealTimeInventoryList(realTimeInventory);
        return getDataTable(list);
    }

    /**
     * 导出即时库存列表
     */
    @PreAuthorize("@ss.hasPermi('warehouse:inventory:export')")
    @Log(title = "即时库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RealTimeInventory realTimeInventory)
    {
        List<RealTimeInventory> list = realTimeInventoryService.selectRealTimeInventoryList(realTimeInventory);
        ExcelUtil<RealTimeInventory> util = new ExcelUtil<RealTimeInventory>(RealTimeInventory.class);
        util.exportExcel(response, list, "即时库存数据");
    }

    /**
     * 获取即时库存详细信息
     */
    @PreAuthorize("@ss.hasPermi('warehouse:inventory:query')")
    @GetMapping(value = "/{aa}")
    public AjaxResult getInfo(@PathVariable("aa") BigDecimal aa)
    {
        return success(realTimeInventoryService.selectRealTimeInventoryByAa(aa));
    }






}
