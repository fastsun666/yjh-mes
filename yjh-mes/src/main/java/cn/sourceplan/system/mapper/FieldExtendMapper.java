package cn.sourceplan.system.mapper;

import java.util.List;

import cn.sourceplan.system.domain.Carousel;
import cn.sourceplan.system.domain.FieldExtend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 字段扩展Mapper接口
 *
 * @author jinzhong
 * @date 2023-05-23
 */
@Repository
public interface FieldExtendMapper extends BaseMapper<FieldExtend>
{

}
