## 🍔 项目介绍

源计划智能工厂MES系统(开源版)

本开源项目为本产品初期DOME版的代码，也会频繁更新，但会落后我们正式版5-6个月的开发进度。

功能包括销售管理，仓库管理，生产管理，质量管理，设备管理，条码追溯，财务管理，系统集成，移动端APP。

## 🛸 技术框架
#### 业务流程

![业务流程图](./doc/screenshot/MESlct.png)

#### 系统架构

![技术架构图](./doc/screenshot/MESjsjg.png)



* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 特别鸣谢：[ruoyi-vue](https://gitee.com/y_project/RuoYi-Vue) ，[JimuReport](https://github.com/jeecgboot/JimuReport)，[element](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FElemeFE%2Felement)，[vue-element-admin](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FPanJiaChen%2Fvue-element-admin)，[eladmin-web](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Felunez%2Feladmin-web)。
* 技术文档：待更新，可以先参考[若依框架](http://doc.ruoyi.vip/ruoyi-vue/)。
* 移动端仓库：[yjh-mes-uniapp](https://gitee.com/Jintiago/yjh-mes-uniapp)

## 🎑 演示环境

演示环境：http://mes.sourceplan.cn/index 

账号：admin 

密码：admin123  请勿修改密码

移动端安装包：将项目Stars后截屏发给微信【**jinzhong373**】获取。

## 🎯 演示图

![](./doc/screenshot/index.png)

![](./doc/screenshot/oee.png)

![](./doc/screenshot/shengchan.png)

![](./doc/screenshot/SNzhuisu.png)

![](./doc/screenshot/shejiqi.png)

![](./doc/screenshot/jicheng.png)

<img src="./doc/screenshot/ydIndex.jpg" width="30%">     <img src="./doc/screenshot/yidongwork.jpg" width="30%">     <img src="./doc/screenshot/ydgongdan.jpg" width="30%">  

<img src="./doc/screenshot/ydbaogong.jpg" width="30%">     <img src="./doc/screenshot/ydlingliao.jpg" width="30%">     <img src="./doc/screenshot/ydweixiu.jpg" width="30%">  

<img src="./doc/screenshot/ydzhijian.jpg" width="30%"> 



## 🤝 商业服务

提供商业服务，团队做过几十家生产制造企业的ERP,WMS,MES系统。

有丰富的硬件数据采集、第三方系统集成(金蝶/用友等)经验。

可以微信联系【**jinzhong373**】。

